const Slack = require('slack-node');
const config = require('config');
const apiToken = config.get("slack.token");
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
  {name: 'username', alias: 'u', type: String, defaultValue: null}, // username of the user
  {name: 'minage', alias: 'm', type: Number}, // time in hours (not tested)
  {name: 'types', alias: 't', type: String, defaultValue: 'images,videos'}, // can be 'all'
  {name: 'all-users', alias : 'a', type : Boolean},
];

const options = commandLineArgs(optionDefinitions);
const slack = new Slack(apiToken);
let result = true;
let countdownFinish = 0;

if (options.username) {
  getUserId(options.username, (err, user) => {
    const userId = user.id;
    deleteFilesWithFilter({userId, types: options.types, minage: options.minage});
  });
} else {

  if (!options['all-users']) {
    console.log('username missing.');
    console.log('If you really want to delete all the file of all users, set the flag --all-users');

    process.exit(1);
  }

  deleteFilesWithFilter({types: options.types, minage: options.minage});
}

function deleteFilesWithFilter({userId, count = 1000, types, minage}) {
  listFiles({user: userId, count: count, types: types}, {minage}, (err, files) => {
    console.log(files.length + ' files to delete');
    countdownFinish = files.length;
    files.forEach((file) => {
      deleteFile(file.id, (err, ok) => {
        if (!ok) {
          console.log('fail to delete file', file.id);
          console.log('cause : ', err);
          result = false;
        }
        finish();
      });
    });
  });
}

function finish() {
  countdownFinish--;
  if (countdownFinish <= 0) {
    if (result) {
      console.log('Everything done ;)');
      process.exit(0);
    }
    process.exit(1);
  }
}

function deleteFile(fileId, cb) {
  slack.api('files.delete', {file: fileId}, (err, response) => {
    cb(err, response.ok);
  });
}

function getUserId(username, cb) {
  slack.api('users.list', (err, response) => {
    cb(err,
      response.members.find((user) => {
        return user.name === username;
      })
    );
  });
}

function listFiles(filter, opt, cb) {

  if (opt && opt.minage) {
    filter.ts_from = tsFromMinAge(opt.minage);
  }

  slack.api('files.list', filter, (err, response) => {
    cb(err, response.files);
  });
}

function tsFromMinAge (minage) {
  return Math.floor(Date.now() / 1000) - (minage * 3600);
}
