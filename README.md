
# The Slack cleaner

This script is not tested. So please understand it before using it.

### Set up

Install the dependencies
```
npm install
```

In `./config`, copy the `default.json` in `local.json` and add your [slack token](https://api.slack.com/custom-integrations/legacy-tokens).


### Run

```
node index.js --username=thomas
```

If you want delete the files older than a week
```
node index.js --username=thomas --minage=168
```

### Arguments

The arguments can be passed like
```
--username=TheUsername
--username TheUsername
-u=TheUsername
-u TheUsername
```

There only one argument mandatory : `username`

##### username (u)

The username of the user you want to delete the file

##### minage (m)

Optional. Default : From the earth creation.
The age (in hours) from which the file will be deleted.

Ex : `--minage=3` will delete all the files created more than 3 hours ago.

##### types (t)

Optional. Default('images,videos')
The file types that will be deleted. 

Ex : `--types=all` delete all the file
